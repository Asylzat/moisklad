<?php
include_once("BitrixElementProperty.php");
class BitrixElement
{
	private $name;
	private $code;
	private $previewPicture;
	private $quantity;
	private $price;
	private $properties;
	private $categories;
	
	/*function BitrixElement($iblockId);
	{
		$this->SetIblockId = $iblockId;
	}
	*/
	
	
	public function SetName($value)
	{
		$this->name = $value;
	}
	public function GetName()
	{
		return $this->name;
	}
	
	public function SetCode($value)
	{
		$this->code = $value;
	}
	public function GetCode()
	{
		return $this->code;
	}
	
	public function SetPreviewPicture($value)
	{
		$this->previewPicture = $value;
	}
	public function GetPreviewPicture()
	{
		return $this->previewPicture;
	}
	
	public function SetQuantity($value)
	{
		$this->quantity = $value;
	}
	public function GetQuantity()
	{
		return intVal($this->quantity);
	}
	
	public function SetPrice($value)
	{
		$this->price = $value;
	}
	public function GetPrice()
	{
		return floatval($this->price);
	}
	
	public function AddProperty($value)
	{
		$this->properties[] = $value;
	}
	public function GetProperties()
	{
		$result = array();
		foreach ($this->properties as $property)
		{
			$result[$property->GetCode()] = $property->GetValue();
		}
		return $result;
	}
	
	public function AddCategories($value)
	{
		$this->categories[] = $value;
	}
	public function GetCategories()
	{
		return $this->categories;
	}
	
}
?>