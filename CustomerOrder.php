<?php

class CustomerOrder {

   private $xml;
   
   function __construct($orderId)
   {
      $this->xml = new SimpleXMLElement("<customerOrder/>");
      $this->xml->addAttribute("vatIncluded", "true");
      $this->xml->addAttribute("applicable",  "true");
      $this->xml->addAttribute("sourceStoreUuid", MoySkladConfig::SKLAD_ID);
      $this->xml->addAttribute("payerVat", "false");
      $this->xml->addAttribute("sourceAgentUuid", MoySkladConfig::CONTRAGENT_ID);
      $this->xml->addAttribute("targetAgentUuid", MoySkladConfig::ORGANIZATION_ID);
//      $this->xml->addAttribute("stateUuid", MoySkladConfig::ORDER_OK_STATE_UUID);
      $this->xml->addAttribute("name", $orderId);
      $this->xml->addChild("ownerUid", "anna@shalom");
   }
   public function addGood($goodUuid, $quantity, $sum)
   {
         $orderPosition = $this->xml->addChild("customerOrderPosition");
         $orderPosition->addAttribute("vat", 0); // НДС
         $orderPosition->addAttribute("goodUuid", $goodUuid);
         $orderPosition->addAttribute("quantity", MoySkladConfig::numberToString($quantity));
         $orderPosition->addAttribute("discount", 0);
         $basePrice = $orderPosition->addChild("basePrice");
         $basePrice->addAttribute("sumInCurrency", MoySkladConfig::numberToString($sum * 100));
         $basePrice->addAttribute("sum", MoySkladConfig::numberToString($sum * 100));
         $price = $orderPosition->addChild("price");
         $price->addAttribute("sumInCurrency", MoySkladConfig::numberToString($sum * 100));
         $price->addAttribute("sum", MoySkladConfig::numberToString($sum * 100));
         $orderPosition->addChild("reserve", MoySkladConfig::numberToString($quantity));
   }

   public function getXML() {
      return $this->xml->asXML();
   }

} // customerOrder
?>
