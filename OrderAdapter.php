<?
require_once('MoySkladService.php');
require_once('CustomerOrder.php');
require_once('MoySkladConfig.php');

/////////////////////////////////////////////////////////


class MoySkladOrderAdapter
{
	static function SetBitrixOrderProperty($orderId, $value)
	{
		CModule::IncludeModule("sale");
		if ($arProp = CSaleOrderProps::GetList(array(), array('CODE' => MoySkladConfig::PROPERTY_MOY_SKLAD_ORDER_CODE))->Fetch()) 
		{
			$add = CSaleOrderPropsValue::Add(array(
			   'NAME' => $arProp['NAME'],
			   'CODE' => $arProp['CODE'],
			   'ORDER_PROPS_ID' => $arProp['ID'],
			   'ORDER_ID' => $orderId,
			   'VALUE' => $value
				)
			);
			if ($arProp = CSaleOrderPropsValue::GetList(array(), array('ORDER_ID' => $orderId, 'CODE' => MoySkladConfig::PROPERTY_MOY_SKLAD_ORDER_CODE))->Fetch())
			{
				return CSaleOrderPropsValue::Update(
					$arProp['ID'],
					array('VALUE' => $value)
				);
			}
		 }
		 return false;
	}
	
	static function AddMoySkladOrder($orderId)
	{
		CModule::IncludeModule("iblock");
		CModule::IncludeModule("sale");
		
		$moySkladOrder = new CustomerOrder($orderId);
		$dbBasketItems = CSaleBasket::GetList(
			array("ID" => "ASC"),
			array("ORDER_ID" => $orderId),
			false,
			false,
			array("PRODUCT_ID", "QUANTITY", "PRICE")
		);
		while ($arItems = $dbBasketItems->GetNext())
		{
			$elementObj = CIBlockElement::GetByID($arItems["PRODUCT_ID"])->GetNextElement();
			$moySkladElementId = $elementObj->GetProperty(MoySkladConfig::PROPERTY_MOY_SKLAD_ELEMENT_CODE);
			$moySkladOrder->addGood($moySkladElementId["VALUE"], $arItems["QUANTITY"], $arItems["PRICE"]);
		}
		
		try 
		{
			$service = new MoySkladService();
			$moySkladOrderId = $service->addOrder($moySkladOrder);
			if (empty($moySkladOrderId))
				file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/upload/logo.txt", file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/upload/logo.txt") . "\r\n" . date() . " Заказ битрекс №" . $orderId . " Ошибка в добавлении заказа в Мой склад. Описание: нет id склада");
			MoySkladOrderAdapter::SetBitrixOrderProperty($orderId, $moySkladOrderId); 
		} 
		catch (Exception $e) 
		{
			file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/upload/logo.txt", file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/upload/logo.txt") . "\r\n" . date() . " Заказ битрекс №" . $orderId . " Ошибка в добавлении заказа в Мой склад. Описание: " . $e->getMessage());
		}
						
	}
	
	static function DeleteMoySkladOrder($orderId)
	{
		CModule::IncludeModule("sale");
		$propObj = CSaleOrderPropsValue::GetList(
			array(),
			array(
			   'CODE' => MoySkladConfig::PROPERTY_MOY_SKLAD_ORDER_CODE,
			   'ORDER_ID' => $orderId
			)
		);
		if ($prop = $propObj->Fetch())
		{
			if (!empty($prop["VALUE"]))
			{
				
				try 
				{
					$service = new MoySkladService();
					$service->deleteOrder($prop["VALUE"]);
					MoySkladOrderAdapter::SetBitrixOrderProperty($orderId, "");
				} 
				catch (Exception $e) 
				{
						file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/upload/logo.txt", file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/upload/logo.txt") . "\r\n" . date() . " Заказ битрекс №" . $orderId . " Ошибка в удалении заказа в Мой склад. Описание: " . $e->getMessage());
				}	
				

			}
		}
	}
	
	static function GetStatusGroup($status)
	{
		if (($status == "N") || ($status == "C")) return 0;
		return 1;
	}
	
	function OnSaleBeforeStatusOrderHandler($orderId, $val)
	{
		CModule::IncludeModule("sale");
		$order = CSaleOrder::GetByID($orderId);
		if (MoySkladOrderAdapter::GetStatusGroup($val) > MoySkladOrderAdapter::GetStatusGroup($order["STATUS_ID"]))
		{
			MoySkladOrderAdapter::AddMoySkladOrder($orderId);
		}
		elseif (MoySkladOrderAdapter::GetStatusGroup($val) < MoySkladOrderAdapter::GetStatusGroup($order["STATUS_ID"]))
		{
			MoySkladOrderAdapter::DeleteMoySkladOrder($orderId);
		}
	}
	
	function OnSaleCancelOrderHandler($orderId, $val)
	{
		if ($val == "Y")
		{
			MoySkladOrderAdapter::DeleteMoySkladOrder($orderId);
		}
		if ($val == "N")
		{
			MoySkladOrderAdapter::AddMoySkladOrder($orderId);
		}
		
	}
	
	function OnBeforeOrderDeleteHandler($orderId)
	{
		MoySkladOrderAdapter::DeleteMoySkladOrder($orderId);
	}
}
AddEventHandler("sale", "OnSaleBeforeStatusOrder", Array("MoySkladOrderAdapter", "OnSaleBeforeStatusOrderHandler"));
AddEventHandler("sale", "OnSaleCancelOrder", Array("MoySkladOrderAdapter", "OnSaleCancelOrderHandler"));
?>