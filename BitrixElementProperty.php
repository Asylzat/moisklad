<?php
class BitrixElementProperty
{
	private $code;
	private $value;
	
	private function SetCode($value)
	{
		$this->code = $value;
	}
	
	private function SetValue($value)
	{
		$this->value = $value;
	}
	
	function BitrixElementProperty($code, $value)
	{
		$this->SetCode($code);
		$this->SetValue($value);
	}
	
	public function GetCode()
	{
		return $this->code;
	}
	
	public function GetValue()
	{
		return $this->value;
	}
}
?>