<?php

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

require_once('BitrixElementLoader.php');
require_once('VerifyElement.php');
require_once('MoySkladService.php');
require_once('MoySkladConfig.php');


$service = new MoySkladService();
//============================================
// Обновление размеров 
//============================================


?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<?php

$categories = $service->getCategories();
$producers = $service->getManufacturies();
$sets = $service->getSets();
$productGroups = array(
	MoySkladConfig::PICTURE_IBLOCK_ID => array(
		"GROUPS" => array(MoySkladConfig::MENGLEI_GROUP_UUID, MoySkladConfig::HOBBART_GROUP_UUID, MoySkladConfig::SCHIPER_GROUP_UUID),
		"VERIFY" => new VerifyElementPicture()
	),
	MoySkladConfig::ACCESSORY_IBLOCK_ID => array(
		"GROUPS" => array(MoySkladConfig::EASELS_GROUP_UUID, MoySkladConfig::BAGUETTES_GROUP_UUID, MoySkladConfig::PAINTS_GROUP_UUID, MoySkladConfig::LACS_GROUP_UUID),
		"VERIFY" => new VerifyElementAccessories()
	),
	MoySkladConfig::MOZAIK_IBLOCK_ID => array(
		"GROUPS" => array(MoySkladConfig::MOZAIK_GROUP_UUID),
		"VERIFY" => new VerifyElementMozaik()
	),
	MoySkladConfig::LENTI_IBLOCK_ID => array(
		"GROUPS" => array(MoySkladConfig::LENTI_GROUP_UUID),
		"VERIFY" => new VerifyElementLenti()
	)
);
//$verifyElementPicture = new VerifyElementPicture();
foreach($productGroups as $iblockId => $arParams)
{
	//if($iblockId != MoySkladConfig::MOZAIK_IBLOCK_ID) continue;
	$bel = new BitrixElementLoader($iblockId, $arParams["VERIFY"]);
	foreach($arParams["GROUPS"] as $groupId)
	{
		$moySkladStocks = $service->getStock($groupId);
		
		// check the total summmm
		$moySkladGoodsAll = $service->getGoodsAll();
		$count = intval($moySkladGoodsAll->attributes()->total/1000);

		if (($moySkladGoodsAll->attributes()->total)%1000 > 0) $count++;

		$arMoySkladGoods = array();

		for ($i = 0;$i <$count ; $i++) {
			$moySkladGoods = $service->getGoods($groupId, 1000 * $i);

		// check the total sum end

		
			foreach ($moySkladGoods as $moySkladGood) 
			{
				$prices = $moySkladGood->salePrices;
				foreach($prices->price as $price)
				{
					$priceAtr = $price->attributes();
					if ((string)$priceAtr["priceTypeUuid"] == MoySkladConfig::PRICE_TYPE_UUID)
					{
						$arMoySkladGoods[(string)$moySkladGood->uuid]["PRICE"] = (float)$priceAtr["value"]/100;
						break;
					}
				}
				
				$attributes = $moySkladGood->attribute;
				$arMoySkladGoods[(string)$moySkladGood->uuid]["SECTIONS"] = array();
				foreach($attributes as $attribute)
				{
					$attributesAtr = $attribute->attributes();
					if ((string)$attributesAtr["metadataUuid"] == MoySkladConfig::SIZE_TYPE_UUID)
					{
						$arMoySkladGoods[(string)$moySkladGood->uuid]["SIZE"] = (string)$attributesAtr["entityValueUuid"];
					}
					if ((string)$attributesAtr["metadataUuid"] == MoySkladConfig::SET_TYPE_UUID)
					{
						foreach ($sets as $set)
						{
							if ((string)$set->uuid != (string)$attributesAtr["entityValueUuid"]) continue;
							
							$arMoySkladGoods[(string)$moySkladGood->uuid]["SET"] = (string)$set->description; 
						}
					}
					if (((string)$attributesAtr["metadataUuid"] == MoySkladConfig::CATEGORY_TYPE_UUID_1) || 
						((string)$attributesAtr["metadataUuid"] == MoySkladConfig::CATEGORY_TYPE_UUID_2) || 
						((string)$attributesAtr["metadataUuid"] == MoySkladConfig::CATEGORY_TYPE_UUID_3))
					{
						foreach ($categories as $category)
						{
							if ((string)$category->uuid != (string)$attributesAtr["entityValueUuid"]) continue;
							
							$arMoySkladGoods[(string)$moySkladGood->uuid]["SECTIONS"][] = (integer)$category->code; 
						}
					}
					if ((string)$attributesAtr["metadataUuid"] == MoySkladConfig::MANUFACTURER_TYPE_UUID)
					{
						foreach ($producers as $producer)
						{
							if ((string)$producer->uuid != (string)$attributesAtr["entityValueUuid"]) continue;
							var_dump($producer->code);
							$arMoySkladGoods[(string)$moySkladGood->uuid]["PRODUCER"] = (integer)$producer->code;
							break;
						}
					}
					if ((string)$attributesAtr["metadataUuid"] == MoySkladConfig::COMPLEXITY_TYPE_UUID)
					{
						$arMoySkladGoods[(string)$moySkladGood->uuid]["COMPLEXITY"] = intVal($attributesAtr["longValue"]);
					}
					if ((string)$attributesAtr["metadataUuid"] == MoySkladConfig::COLOR_COUNT_TYPE_UUID)
					{
						$arMoySkladGoods[(string)$moySkladGood->uuid]["COLOR_COUNT"] = intVal($attributesAtr["longValue"]);
					}
				}

			}

		}
		$params = Array(
		   "max_len" => "100", // обрезает символьный код до 100 символов
		   "change_case" => "L", // буквы преобразуются к нижнему регистру
		   "replace_space" => "-", // меняем пробелы на нижнее подчеркивание
		   "replace_other" => "-", // меняем левые символы на нижнее подчеркивание
		   "delete_repeat_replace" => "true", // удаляем повторяющиеся нижние подчеркивания
		);

		
		foreach ($moySkladStocks as $moySkladStock) 
		{
			$bitrixElement = new BitrixElement();
			if($groupId == MoySkladConfig::EASELS_GROUP_UUID)
				$bitrixElement->AddCategories(17);
			if($groupId == MoySkladConfig::PAINTS_GROUP_UUID)
				$bitrixElement->AddCategories(15);
			if($groupId == MoySkladConfig::LACS_GROUP_UUID)
				$bitrixElement->AddCategories(16);
			if($groupId == MoySkladConfig::BAGUETTES_GROUP_UUID)
				$bitrixElement->AddCategories(34);
				
			$stokAtr = $moySkladStock->attributes();
			
			if ((int)$stokAtr["quantity"] < 1)
				$bitrixElement->SetQuantity(0);
			else
				$bitrixElement->SetQuantity((int)$stokAtr["quantity"]);
				
			$bitrixElement->AddProperty(new BitrixElementProperty(MoySkladConfig::PROPERTY_ARTICUL_CODE, (string)$stokAtr["productCode"]));
			if (file_exists($_SERVER["DOCUMENT_ROOT"] . MoySkladConfig::PATH_PHOTO . strtolower((string)$stokAtr["productCode"]) . ".jpg"))
			{	
				$bitrixElement->AddProperty(new BitrixElementProperty(MoySkladConfig::PROPERTY_MORE_PHOTO_CODE, array(CFile::MakeFileArray(MoySkladConfig::PATH_PHOTO . strtolower((string)$stokAtr["productCode"]) . ".jpg"))));
			}
			if (file_exists($_SERVER["DOCUMENT_ROOT"] . MoySkladConfig::PATH_PHOTO . strtolower((string)$stokAtr["productCode"]) . ".png"))
			{	
				$bitrixElement->AddProperty(new BitrixElementProperty(MoySkladConfig::PROPERTY_MORE_PHOTO_CODE, array(CFile::MakeFileArray(MoySkladConfig::PATH_PHOTO . strtolower((string)$stokAtr["productCode"]) . ".png"))));
			}
			if (file_exists($_SERVER["DOCUMENT_ROOT"] . MoySkladConfig::PATH_PHOTO_SMALL . strtolower((string)$stokAtr["productCode"]) . ".jpg"))
			{	
				$bitrixElement->SetPreviewPicture(array(CFile::MakeFileArray(MoySkladConfig::PATH_PHOTO_SMALL . strtolower((string)$stokAtr["productCode"]) . ".jpg")));
			}
			if (file_exists($_SERVER["DOCUMENT_ROOT"] . MoySkladConfig::PATH_PHOTO_SMALL . strtolower((string)$stokAtr["productCode"]) . ".png"))
			{	
				$bitrixElement->SetPreviewPicture(array(CFile::MakeFileArray(MoySkladConfig::PATH_PHOTO_SMALL . strtolower((string)$stokAtr["productCode"]) . ".png")));
			}
			
			$stokGoodRefAtr = $moySkladStock->goodRef->attributes();
			$bitrixElement->AddProperty(new BitrixElementProperty(MoySkladConfig::PROPERTY_MY_SKLAD_ELEMENT_ID_CODE, (string)$stokGoodRefAtr["uuid"]));
			$bitrixElement->SetName((string)$stokGoodRefAtr["name"]);
			$bitrixElement->SetPrice($arMoySkladGoods[(string)$stokGoodRefAtr["uuid"]]["PRICE"]);
			foreach ($arMoySkladGoods[(string)$stokGoodRefAtr["uuid"]]["SECTIONS"] as $section)
			{
				$bitrixElement->AddCategories($section);
			}
			$bitrixElement->AddProperty(new BitrixElementProperty(MoySkladConfig::PROPERTY_SIZE_CODE, $bitrixSizes[$arMoySkladGoods[(string)$stokGoodRefAtr["uuid"]]["SIZE"]]));
			$bitrixElement->AddProperty(new BitrixElementProperty(MoySkladConfig::PROPERTY_PRODUCER_CODE, $arMoySkladGoods[(string)$stokGoodRefAtr["uuid"]]["PRODUCER"]));


			$bitrixElement->AddProperty(new BitrixElementProperty(MoySkladConfig::PROPERTY_SET_CODE, $arMoySkladGoods[(string)$stokGoodRefAtr["uuid"]]["SET"]));
			//$bitrixElement->AddProperty(new BitrixElementProperty(MoySkladConfig::PROPERTY_COMPLEXITY_CODE, $actualComplexities[$iblockId][$arMoySkladGoods[(string)$stokGoodRefAtr["uuid"]]["COMPLEXITY"]]));
			$bitrixElement->AddProperty(new BitrixElementProperty(MoySkladConfig::PROPERTY_COLOR_COUNT_CODE, $arMoySkladGoods[(string)$stokGoodRefAtr["uuid"]]["COLOR_COUNT"]));
			//$bitrixElement->SetCode(CUtil::translit((string)$stokGoodRefAtr["name"] . " " . (string)$stokAtr["productCode"], "ru", $params));
			$bel->AddElement($bitrixElement);
			if ($bitrixElement->GetCategories()<1) echo "<div style='background:yellow;'>"; var_dump($groupId);echo "</div>";

			echo "<pre> Array - ";var_dump($bitrixElement);echo "</pre>";
			
		}

		/*print("<pre>");
		print_r($bitrixElement);
		print("</pre>");*/

	}
	$bel->LoadInBitrix();
}

$verifyElement = '';
foreach($productGroups as $iblockId => $arParams)
{
	$verifyElement .= $arParams["VERIFY"]->GetErrors() . "<br>";
}



var_dump($verifyElement);

?>
</body>
</html>