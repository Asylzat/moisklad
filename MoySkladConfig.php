<?php
class MoySkladConfig {
	const URL = "ssl://online.moysklad.ru";
	const PORT = 443;
	const AUTH = "admin@shalom:28g03g2011";
	const SKLAD_ID = "689a000c-a14e-11e2-9030-001b21d91495";
	const CONTRAGENT_ID = "dd0e9c55-a396-11e2-7d94-001b21d91495";
	const ORGANIZATION_ID = "6895f876-a14e-11e2-b23c-001b21d91495";
	const ORDER_OK_STATE_UUID = "dd675d83-a396-11e2-c56e-001b21d91495";

	const PRICE_TYPE_UUID = "5bcf63c7-ae65-11e2-283e-001b21d91495";

	const SIZES_METADATA_UUID = "e3d3723a-e3d7-11e2-cbba-7054d21a8d1e";			   // Размеры товаров
	const SIZE_TYPE_UUID = "2da66e46-e3d8-11e2-9b00-7054d21a8d1e";

	const CATEGORIES_METADATA_UUID = "2dd9fda5-e3cf-11e2-9076-7054d21a8d1e";       // Категории товаров
	const CATEGORY_TYPE_UUID_1 = "2da67490-e3d8-11e2-0fe5-7054d21a8d1e";            
	const CATEGORY_TYPE_UUID_2 = "2da6777d-e3d8-11e2-3c1f-7054d21a8d1e";
	const CATEGORY_TYPE_UUID_3 = "f26391b2-e93b-11e2-ec33-7054d21a8d1e";

	const MANUFACTURIES_METADATA_UUID = "6235251e-e3d7-11e2-0de2-7054d21a8d1e";
	const MANUFACTURER_TYPE_UUID = "2da67148-e3d8-11e2-c954-7054d21a8d1e";         // Производитель

	const SETS_METADATA_UUID = "411726a9-9e20-11e3-ad4f-002590a28eca";
	const SET_TYPE_UUID = "5bf89ef6-9e20-11e3-78ee-002590a28eca";                  // Комплектация(описание коплектации через ";")

	const TYPES_METADATA_UUID = "1f0d12c7-9e20-11e3-2d2a-002590a28eca";
	const TYPE_TYPE_UUID = "5bf89df4-9e20-11e3-d5b5-002590a28eca";                 // Тип (на холсте; на картоне)

	const COMPLEXITY_TYPE_UUID = "5bf89bf0-9e20-11e3-a921-002590a28eca";           // Сложность (1 - 5)
	const COLOR_COUNT_TYPE_UUID = "bddac08b-9e20-11e3-c598-002590a28eca";          // Количесво цветов (1-50)

	const MENGLEI_GROUP_UUID = "8e21b3fc-a14e-11e2-075a-001b21d91495";			   // Группы товаров
	const HOBBART_GROUP_UUID = "66f4cdfd-1ed1-11e3-0bb6-7054d21a8d1e";
	const SCHIPER_GROUP_UUID = "67a0514e-363b-11e3-ccf0-7054d21a8d1e";
	const EASELS_GROUP_UUID = "a5099309-8800-11e3-f27a-002590a28eca";
	const BAGUETTES_GROUP_UUID = "34638f6b-757c-11e4-90a2-8eca0011f79a";
	const PAINTS_GROUP_UUID = "17ae864b-757c-11e4-90a2-8eca0011f2b7";
	const LACS_GROUP_UUID = "2921859d-757c-11e4-90a2-8eca0011f4a1";
    const MOZAIK_GROUP_UUID = "ec068ad5-4d40-11e4-90a2-8eca00484fb8";
    const LENTI_GROUP_UUID = "e64ea72b-e6ee-11e5-7a69-9711003a4abb";

	
	//Параметры битрикс
	const PICTURE_IBLOCK_ID = 1;
	const ACCESSORY_IBLOCK_ID = 7;
	const PRODUCER_IBLOCK_ID = 6;
	const SIZE_IBLOCK_ID = 18;
	const MOZAIK_IBLOCK_ID = 19;
	const LENTI_IBLOCK_ID = 20;

	
	const PROPERTY_MOY_SKLAD_UUID = "MOY_SKLAD_UUID";
	const PROPERTY_ARTICUL_CODE = "ARTICUL";
	const PROPERTY_MY_SKLAD_ELEMENT_ID_CODE = "MY_SKLAD_ELEMENT_ID";
	const PROPERTY_SIZE_CODE = "SIZE2";
	const PROPERTY_PRODUCER_CODE = "PRODUCER";
	const PROPERTY_MORE_PHOTO_CODE = "MORE_PHOTO";
	const PROPERTY_SET_CODE = "SET";
	const PROPERTY_COMPLEXITY_CODE = "COMPLEXITY";
	const PROPERTY_COLOR_COUNT_CODE = "COLOR_COUNT";
	
	
	
	
	const PROPERTY_ARTICUL_ID = 1;
	const PROPERTY_SIZE_ID = 2;
	const PROPERTY_TYPE_ID = 3;
	const PROPERTY_COMPLECT_ID = 4;
	const PROPERTY_MORE_PHOTO_ID = 6;
	const PROPERTY_PRODUCER_ID = 12;
	const PROPERTY_COMPLEXITY_ID = 36;
	const PATH_PHOTO = "/upload/moy_sklad_photo/";
	const PATH_PHOTO_SMALL = "/upload/moy_sklad_photo/small/";
	const PROPERTY_MOY_SKLAD_ORDER_CODE = "MOY_SKLAD_ORDER_ID";
	const PROPERTY_MOY_SKLAD_ELEMENT_CODE = "MY_SKLAD_ELEMENT_ID";
	const PROPERTY_MY_SKLAD_ELEMENT_ID = "32";

	static public function numberToString($number)
	{
		return number_format($number, 2, '.', '');
	}
}
?>
