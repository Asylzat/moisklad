<?php 
require_once('MoySkladConfig.php');
require_once('CustomerOrder.php');
require_once('Utils.php');

class MoySkladService {

   private $entitiesIsLoad = false;
   private $categories = array();
   private $sizes = array();
   private $manufacturies = array();
   private $sets = array();
   private $types = array();

   private function sendRequest($url, $method, $body = '')
   {
      $sock = fsockopen(MoySkladConfig::URL, MoySkladConfig::PORT, $errno, $errstr, 300);

      if (!$sock) die("$errstr ($errno)\n");

      fputs($sock, $method . " " . $url . " HTTP/1.1\r\n");
      fputs($sock, "Host: online.moysklad.ru\r\n");
      fputs($sock, "Authorization: Basic " . base64_encode(MoySkladConfig::AUTH) . "\r\n");
      fputs($sock, "Content-Type: application/xml; charset=utf-8 \r\n");
      fputs($sock, "Accept: */*\r\n");
      fputs($sock, "Content-Length: ". mb_strlen($body) ."\r\n");
      fputs($sock, "Connection: close\r\n\r\n");
      fputs($sock, "$body" . "\r\n\r\n");
    
      while ($str = trim(fgets($sock, 4096)));

      $result = '';//stream_get_contents($sock);
    
      while (!feof($sock)) {
         $result .= fgets($sock, 4096);
      }
      fclose($sock);
      //var_dump($result);
      return http_chunked_decode($result);
	  
   }

   public function addOrder($order)
   {
      $result = $this->sendRequest("/exchange/rest/ms/xml/CustomerOrder", "PUT", $order->getXML());
      $xml = new SimpleXMLElement($result);
	   return $xml->uuid;
   }

   public function deleteOrder($orderId)
   {
      return $this->sendRequest("/exchange/rest/ms/xml/CustomerOrder/" . $orderId, "DELETE", '');
   }

   public function getStock($groupId)
   {
      return new SimpleXMLElement($this->sendRequest("/exchange/rest/stock/xml?storeId=" . MoySkladConfig::SKLAD_ID . "&goodUuid=" . $groupId, "GET"));
   }

   public function getGoods($groupId, $start = 0)
   {
      return new SimpleXMLElement($this->sendRequest("/exchange/rest/ms/xml/Good/list?start=" . $start . "&filter=parentUuid%3D" . $groupId, "GET"));
   }
	public function getGoodsAll($start = 0)
   {
      return new SimpleXMLElement($this->sendRequest("/exchange/rest/ms/xml/Good/list?start=" . $start, "GET"));
   }

   private function loadEntities()
   {
      $result = new SimpleXMLElement($this->sendRequest("/exchange/rest/ms/xml/CustomEntity/list", "GET"));
      foreach($result->customEntity as $entity) {
         switch($entity->attributes()->entityMetadataUuid) {
            case MoySkladConfig::SIZES_METADATA_UUID:
               $this->sizes[] = $entity;
               break;
            case MoySkladConfig::CATEGORIES_METADATA_UUID:
               $this->categories[] = $entity;
               break;
            case MoySkladConfig::MANUFACTURIES_METADATA_UUID:
               $this->manufacturies[] = $entity;
               break; 
			case MoySkladConfig::SETS_METADATA_UUID:
			   $this->sets[] = $entity;
               break; 
			case MoySkladConfig::TYPES_METADATA_UUID:
			   $this->types[] = $entity;
               break; 
         }
      }


      array_unshift($this->sizes,""); 
      unset($this->sizes[0]);

      array_unshift($this->categories,""); 
      unset($this->categories[0]);

      array_unshift($this->manufacturies,""); 
      unset($this->manufacturies[0]);

      array_unshift($this->sets,""); 
      unset($this->sets[0]);

      array_unshift($this->types,""); 
      unset($this->types[0]);

      $this->entitiesIsLoad = true;
   }

   public function getSizes()
   {
      if (!$this->entitiesIsLoad) {
         $this->loadEntities();
      }
      return $this->sizes;
   }

   public function getCategories()
   {
      if (!$this->entitiesIsLoad) {
         $this->loadEntities();
      }
      return $this->categories;
   }

   public function getManufacturies()
   {
      if (!$this->entitiesIsLoad) {
         $this->loadEntities();
      }
      return $this->manufacturies;
   }

   public function getSets()
   {
      if (!$this->entitiesIsLoad) {
         $this->loadEntities();
      }
      return $this->sets;
   }

   public function getTypes()
   {
      if (!$this->entitiesIsLoad) {
         $this->loadEntities();
      }
      return $this->types;
   }


} // MoySkladRequest
?>
