<?php
include_once("BitrixElement.php");
require_once('MoySkladConfig.php');
abstract class VerifyElement
{
	private $errors = "";
	public function AddError($value)
	{
		$this->errors .= $value;
	}
	public function GetErrors()
	{
		return $this->errors;
	}
	public function IsVerify($element)
	{
		return;
	}
}
class VerifyElementPicture extends VerifyElement
{
	function IsVerify($element)
	{
		$isError = false;
		$error = "\r\n" ."==================Картина==========================";
		$properties = $element->GetProperties();
		if (empty($properties[MoySkladConfig::PROPERTY_ARTICUL_CODE]))
		{
			$isError = true;
			$error .= "\r\n" . "У элемента нет артикула";
		}
		$name = $element->GetName();
		if(empty($name))
		{
			$isError = true;
			$error .= "\r\n" . $properties[MoySkladConfig::PROPERTY_ARTICUL_CODE] . " - У элемента нет наименования";
		}
		if(empty($properties[MoySkladConfig::PROPERTY_MORE_PHOTO_CODE]))
		{
			$isError = true;
			$error .= "\r\n" . $properties[MoySkladConfig::PROPERTY_ARTICUL_CODE] . " - У элемента '" . $name . "' нет изображения";	
		}
		if(empty($properties[MoySkladConfig::PROPERTY_PRODUCER_CODE]))
		{
			$isError = true;
			$error .= "\r\n" . $properties[MoySkladConfig::PROPERTY_ARTICUL_CODE] . " - У элемента '" . $name . "' нет производителя";	
		}
		if(empty($properties[MoySkladConfig::PROPERTY_MY_SKLAD_ELEMENT_ID_CODE]))
		{
			$isError = true;
			$error .= "\r\n" . $properties[MoySkladConfig::PROPERTY_ARTICUL_CODE] . " - У элемента '" . $name . "' нет ID элемента из 'мой склад'";	
		}
		if(empty($properties[MoySkladConfig::PROPERTY_SIZE_CODE]))
		{
			$isError = true;
			$error .= "\r\n" . $properties[MoySkladConfig::PROPERTY_ARTICUL_CODE] . " - У элемента '" . $name . "' нет размера";	
		}
		if(count($element->GetCategories()) < 1)
		{
			$isError = true;
			$error .= "\r\n" . $properties[MoySkladConfig::PROPERTY_ARTICUL_CODE] . " - У элемента '" . $name . "' нет привязок к папкам";	
		}
		if($element->GetPrice() == 0)
		{
			$isError = true;
			$error .= "\r\n" . $properties[MoySkladConfig::PROPERTY_ARTICUL_CODE] . " - У элемента '" . $name . "' не может быть цены равной 0";	
		}
		$error .= "\r\n" ."======================================================";
		if($isError)
		{
			$this->AddError($error);
			return false;
		}
		return true;
	}
}

class VerifyElementAccessories extends VerifyElement
{
	function IsVerify($element)
	{
		$isError = false;
		$error = "\r\n" ."================Аксессуар====================";
		$properties = $element->GetProperties();
		if (empty($properties[MoySkladConfig::PROPERTY_ARTICUL_CODE]))
		{
			$isError = true;
			$error .= "\r\n" . "У элемента нет артикула";
		}
		$name = $element->GetName();
		if(empty($name))
		{
			$isError = true;
			$error .= "\r\n" . $properties[MoySkladConfig::PROPERTY_ARTICUL_CODE] . " - У элемента нет наименования";
		}
		if(empty($properties[MoySkladConfig::PROPERTY_MORE_PHOTO_CODE]))
		{
			$isError = true;
			$error .= "\r\n" . $properties[MoySkladConfig::PROPERTY_ARTICUL_CODE] . " - У элемента '" . $name . "' нет изображения";	
		}
		/*if(empty($properties[MoySkladConfig::PROPERTY_PRODUCER_CODE]))
		{
			$isError = true;
			$error .= "\r\n" . $properties[MoySkladConfig::PROPERTY_ARTICUL_CODE] . " - У элемента '" . $name . "' нет производителя";	
		}*/
		if(empty($properties[MoySkladConfig::PROPERTY_MY_SKLAD_ELEMENT_ID_CODE]))
		{
			$isError = true;
			$error .= "\r\n" . $properties[MoySkladConfig::PROPERTY_ARTICUL_CODE] . " - У элемента '" . $name . "' нет ID элемента из 'мой склад'";	
		}
		if(empty($properties[MoySkladConfig::PROPERTY_SIZE_CODE]))
		{
			$isError = true;
			$error .= "\r\n" . $properties[MoySkladConfig::PROPERTY_ARTICUL_CODE] . " - У элемента '" . $name . "' нет размера";	
		}
		if(count($element->GetCategories()) < 1)
		{
			$isError = true;
			$error .= "\r\n" . $properties[MoySkladConfig::PROPERTY_ARTICUL_CODE] . " - У элемента '" . $name . "' нет привязок к папкам";	
		}
		if($element->GetPrice() == 0)
		{
			$isError = true;
			$error .= "\r\n" . $properties[MoySkladConfig::PROPERTY_ARTICUL_CODE] . " - У элемента '" . $name . "' не может быть цены равной 0";	
		}
		$error .= "\r\n" ."======================================================";
		if($isError)
		{
			$this->AddError($error);
			return false;
		}
		return true;
	}
}

class VerifyElementMozaik extends VerifyElement
{
	function IsVerify($element)
	{
		$isError = false;
		$error = "\r\n" ."==================Мозайка==========================";
		$properties = $element->GetProperties();
		if (empty($properties[MoySkladConfig::PROPERTY_ARTICUL_CODE]))
		{
			$isError = true;
			$error .= "\r\n" . "У элемента нет артикула";
		}
		$name = $element->GetName();
		if(empty($name))
		{
			$isError = true;
			$error .= "\r\n" . $properties[MoySkladConfig::PROPERTY_ARTICUL_CODE] . " - У элемента нет наименования";
		}
		if(empty($properties[MoySkladConfig::PROPERTY_MORE_PHOTO_CODE]))
		{
			$isError = true;
			$error .= "\r\n" . $properties[MoySkladConfig::PROPERTY_ARTICUL_CODE] . " - У элемента '" . $name . "' нет изображения";
		}
		if(empty($properties[MoySkladConfig::PROPERTY_PRODUCER_CODE]))
		{
			$isError = true;
			$error .= "\r\n" . $properties[MoySkladConfig::PROPERTY_ARTICUL_CODE] . " - У элемента '" . $name . "' нет производителя";echo "<div style='background:yellow;'>"; var_dump($properties[MoySkladConfig::PROPERTY_PRODUCER_CODE]);echo "</div>";
		}
		if(empty($properties[MoySkladConfig::PROPERTY_MY_SKLAD_ELEMENT_ID_CODE]))
		{
			$isError = true;
			$error .= "\r\n" . $properties[MoySkladConfig::PROPERTY_ARTICUL_CODE] . " - У элемента '" . $name . "' нет ID элемента из 'мой склад'";
		}
		if(empty($properties[MoySkladConfig::PROPERTY_SIZE_CODE]))
		{
			$isError = true;
			$error .= "\r\n" . $properties[MoySkladConfig::PROPERTY_ARTICUL_CODE] . " - У элемента '" . $name . "' нет размера";
		}
		if(count($element->GetCategories()) < 1)
		{
			$isError = true;
			$error .= "\r\n" . $properties[MoySkladConfig::PROPERTY_ARTICUL_CODE] . " - У элемента '" . $name . "' нет привязок к папкам";
		}
		if($element->GetPrice() == 0)
		{
			$isError = true;
			$error .= "\r\n" . $properties[MoySkladConfig::PROPERTY_ARTICUL_CODE] . " - У элемента '" . $name . "' не может быть цены равной 0";
		}
		$error .= "\r\n" ."======================================================";
		if($isError)
		{
			$this->AddError($error);
			return false;
		}
		return true;
	}
}
class VerifyElementLenti extends VerifyElement
{
	function IsVerify($element)
	{
		$isError = false;
		$error = "\r\n" ."==================Ленты==========================";
		$properties = $element->GetProperties();
		if (empty($properties[MoySkladConfig::PROPERTY_ARTICUL_CODE]))
		{
			$isError = true;
			$error .= "\r\n" . "У элемента нет артикула";
		}
		$name = $element->GetName();
		if(empty($name))
		{
			$isError = true;
			$error .= "\r\n" . $properties[MoySkladConfig::PROPERTY_ARTICUL_CODE] . " - У элемента нет наименования";
		}
		if(empty($properties[MoySkladConfig::PROPERTY_MORE_PHOTO_CODE]))
		{
			$isError = true;
			$error .= "\r\n" . $properties[MoySkladConfig::PROPERTY_ARTICUL_CODE] . " - У элемента '" . $name . "' нет изображения";
		}
		if(empty($properties[MoySkladConfig::PROPERTY_PRODUCER_CODE]))
		{
			$isError = true;
			$error .= "\r\n" . $properties[MoySkladConfig::PROPERTY_ARTICUL_CODE] . " - У элемента '" . $name . "' нет производителя";echo "<div style='background:yellow;'>"; var_dump($properties[MoySkladConfig::PROPERTY_PRODUCER_CODE]);echo "</div>";
		}
		if(empty($properties[MoySkladConfig::PROPERTY_MY_SKLAD_ELEMENT_ID_CODE]))
		{
			$isError = true;
			$error .= "\r\n" . $properties[MoySkladConfig::PROPERTY_ARTICUL_CODE] . " - У элемента '" . $name . "' нет ID элемента из 'мой склад'";
		}
		/*
		if(empty($properties[MoySkladConfig::PROPERTY_SIZE_CODE]))
		{
			$isError = true;
			$error .= "\r\n" . $properties[MoySkladConfig::PROPERTY_ARTICUL_CODE] . " - У элемента '" . $name . "' нет размера";
		}
		*/
		if(count($element->GetCategories()) < 1)
		{
			$isError = true;
			$error .= "\r\n" . $properties[MoySkladConfig::PROPERTY_ARTICUL_CODE] . " - У элемента '" . $name . "' нет привязок к папкам";
		}
		if($element->GetPrice() == 0)
		{
			$isError = true;
			$error .= "\r\n" . $properties[MoySkladConfig::PROPERTY_ARTICUL_CODE] . " - У элемента '" . $name . "' не может быть цены равной 0";
		}
		$error .= "\r\n" ."======================================================";
		if($isError)
		{
			$this->AddError($error);
			return false;
		}
		return true;
	}
}
?>